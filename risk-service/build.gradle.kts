plugins {
    java
    id("io.freefair.lombok") version "6.4.3"
    id("org.springframework.boot") version "2.6.8"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
}

tasks.compileJava {
    options.release.set(17)
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":contracts"))

    implementation("org.springframework.boot:spring-boot-starter")

    implementation(platform("org.camunda.bpm:camunda-bom:7.17.0"))
    implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-external-task-client")
    implementation("javax.xml.bind:jaxb-api:2.3.1") //without this camunda client said: NoClassDefFoundError: javax/xml/bind/JAXBException

}
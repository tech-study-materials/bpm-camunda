package bpm.risk;

import bpm.contracts.risk.RiskCheckResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map;

@SpringBootApplication
@ExternalTaskSubscription("check-risk-requests")
@Slf4j
class RiskService implements ExternalTaskHandler {

    public static void main(String[] args) {
        SpringApplication.run(RiskService.class, args);
    }

    @SneakyThrows
    @Override
    public void execute(ExternalTask task, ExternalTaskService camunda) {
        String transferId = task.getBusinessKey();
        log.info("Evaluating if transferID {} is risky...", transferId);
        boolean risky = transferId.contains("risky");
        camunda.complete(task, Map.of(
                "riskCheckResponse",
                new RiskCheckResponse(transferId, risky ? randomPercent() : 0)));
    }

    private int randomPercent() {
        return (int)(Math.random()*100);
    }
}

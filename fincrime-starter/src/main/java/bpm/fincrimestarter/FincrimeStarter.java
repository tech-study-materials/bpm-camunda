package bpm.fincrimestarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FincrimeStarter {

    public static void main(String[] args) {
        SpringApplication.run(FincrimeStarter.class, args);
    }
}

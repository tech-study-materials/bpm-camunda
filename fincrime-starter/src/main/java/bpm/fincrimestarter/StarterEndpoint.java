package bpm.fincrimestarter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;

import static java.net.http.HttpResponse.BodyHandlers.ofString;

@Slf4j
@RestController
class StarterEndpoint {

    static final HttpClient http = HttpClient.newHttpClient();

    @Value("${url.engine-rest.fincrime-workflow-start}") String workflowStartUrl;

    @PostMapping("/fincrime-workflow-starter/funds-captured/{transferId}")
    void fundsCaptured(@PathVariable String transferId) throws Exception {
        log.info("Funds captured for {}, starting fincrime workflow", transferId);
        var response = http.send(startFincrimeWorkflowFor(transferId), ofString());
        log.info("Camunda responded : HTTP {} : {}", response.statusCode(), response.body());
        if(response.statusCode() != 200) throw new RuntimeException("Camunda did not accept");
    }

    private HttpRequest startFincrimeWorkflowFor(String transferId) throws URISyntaxException {
        return HttpRequest
                .newBuilder()
                .uri(new URI(workflowStartUrl))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString("""
                        {
                            "businessKey" : "%s"
                        }
                        """.formatted(transferId)))
                .build();
    }
}

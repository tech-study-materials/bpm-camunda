package bpm.document;

import bpm.contracts.document.DocumentCheckResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@SpringBootApplication
class DocumentService {

    public static void main(String[] args) {
        SpringApplication.run(DocumentService.class, args);
    }

    @GetMapping("/document-service/check/{transferId}")
    DocumentCheckResponse checkDocument(@PathVariable String transferId) {
        log.info("Evaluating if sender's document of transferID {} is invalid...", transferId);
        return new DocumentCheckResponse(transferId, transferId.contains("invalid"));
    }
}


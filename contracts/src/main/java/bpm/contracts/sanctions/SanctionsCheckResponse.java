package bpm.contracts.sanctions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public
class SanctionsCheckResponse {
    String transferId;
    boolean isSanctioned;
}

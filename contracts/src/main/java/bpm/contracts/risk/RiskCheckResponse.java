package bpm.contracts.risk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RiskCheckResponse {
    String transferId;
    int riskFactor;
}

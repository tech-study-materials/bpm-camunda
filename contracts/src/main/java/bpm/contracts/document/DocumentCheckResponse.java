package bpm.contracts.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DocumentCheckResponse {
    String transferId;
    boolean invalidDocument;
}

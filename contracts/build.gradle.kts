plugins {
    `java-library`
    id("io.freefair.lombok") version "6.4.3"
}

tasks.compileJava {
    options.release.set(17)
}

repositories {
    mavenCentral()
}
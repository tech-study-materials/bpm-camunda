package bpm.jms;

import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.broker.BrokerService;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class JmsBroker {

    public static void main(String[] args) {
        SpringApplication.run(JmsBroker.class, args);
    }

    @Bean
    public BrokerService activemq() throws Exception {
        BrokerService broker = new BrokerService();
        broker.addConnector("tcp://localhost:8079");
        broker.setPersistent(false);
        broker.setUseJmx(false);
        return broker;
    }

    @Bean
    ApplicationRunner runme(BrokerService jms) {
        return args -> {
            log.info("jms broker running: {}", jms.getTransportConnectors());
            jms.waitUntilStopped();
        };
    }
}

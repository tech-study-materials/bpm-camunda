plugins {
    `java-library`
    id("org.openapi.generator") version "6.0.0"
}

tasks.compileJava {
    options.release.set(17)
}

repositories {
    mavenCentral()
}

val camundaOpenApi by configurations.creating
dependencies {
    camundaOpenApi("org.camunda.bpm:camunda-engine-rest-openapi:7.17.0")

    implementation("org.openapitools:jackson-databind-nullable:0.2.2")
    implementation("com.google.code.findbugs:jsr305:3.0.2")

    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.3")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jdk8:2.13.3")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.3")
    implementation("io.swagger:swagger-annotations:1.6.6")

}

val unpackApiDefinition by tasks.creating(Copy::class) {
    group = "openapi tools"
    from(zipTree(camundaOpenApi.singleFile))
    include("/openapi.json")
    into(layout.buildDirectory)
}

val openApiGenerationDir = "$buildDir/generated"
val openApiGeneratedSrcDir = "${openApiGenerationDir}/src/main/java"
sourceSets.main {
    java.srcDir(openApiGeneratedSrcDir)
}

openApiGenerate {
    generatorName.set("java")
    validateSpec.set(true)
    library.set("native")
    inputSpec.set("$buildDir/openapi.json")
    outputDir.set(openApiGenerationDir)
    apiPackage.set("bpm.camunda-client.api")
    invokerPackage.set("bpm.camunda-client.invoker")
    modelPackage.set("bpm.camunda-client.model")
    generateModelTests.set(false)
    generateApiTests.set(false)
    generateModelDocumentation.set(false)
    generateApiDocumentation.set(false)

    configOptions.set(mapOf(
        "dateLibrary" to "java8",
    ))
}

tasks.openApiGenerate {
    dependsOn(unpackApiDefinition)
}
tasks.compileJava {
    dependsOn(tasks.openApiGenerate)
}
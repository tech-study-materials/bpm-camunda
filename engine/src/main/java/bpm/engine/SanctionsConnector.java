package bpm.engine;

import bpm.contracts.sanctions.SanctionsCheckRequest;
import bpm.contracts.sanctions.SanctionsCheckResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.stereotype.Component;

@Component("checkSanctionsServiceTask")
@Slf4j
@RequiredArgsConstructor
class CamundaToJmsBridge implements JavaDelegate {

    final JmsTemplate jmsTemplate;

    @Override
    public void execute(DelegateExecution execution) {
        String transferId = execution.getProcessBusinessKey();
        log.info("Publishing sanctions check request for transfer: {}", transferId);
        jmsTemplate.convertAndSend(
                "sanctions-check-requests",
                new SanctionsCheckRequest(transferId)
        );
    }
}

@Component
@Slf4j
@RequiredArgsConstructor
class JmsToCamundaBridge {

    final ProcessEngine camunda;

    @JmsListener(destination = "sanctions-check-responses")
    void onMessage(SanctionsCheckResponse msg) {
        log.info("Sanctions check response received: {}", msg);
        camunda
                .getRuntimeService()
                .createMessageCorrelation("sanctions-check-results")
                .processInstanceBusinessKey(msg.getTransferId())
                .setVariable("sanctionsCheckResponse", msg)
                .correlate();
    }
}

@Configuration
class JmsConfig {
    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }
}
package bpm.engine;

import bpm.contracts.document.DocumentCheckResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component("documentCheckResponseParser")
@RequiredArgsConstructor
public class DocumentCheckResponseParser {

    final ObjectMapper jackson;

    @SuppressWarnings("unused")
    public DocumentCheckResponse parse(String documentCheckResponseJson) throws JsonProcessingException {
        return jackson.readValue(documentCheckResponseJson, DocumentCheckResponse.class);
    }
}

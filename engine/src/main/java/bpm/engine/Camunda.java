package bpm.engine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class Camunda {

    public static void main(String[] args) {
        SpringApplication.run(Camunda.class, args);
    }
}

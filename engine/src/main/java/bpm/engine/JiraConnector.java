package bpm.engine;

import feign.Feign;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import feign.http2client.Http2Client;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.joining;

@Component("createJiraServiceTask")
@Slf4j
@RequiredArgsConstructor
class CamundaToJiraBridge implements JavaDelegate {

    final Jira jira;

    @Override
    public void execute(DelegateExecution execution) {
        var issueDescription = execution.getVariables().entrySet().stream()
                .map(e -> "%s:%s".formatted(e.getKey(), e.getValue()))
                .collect(joining("\n"));
        var jiraId = jira.createIssue(execution.getProcessBusinessKey(), issueDescription).id;
        execution.setVariable("jiraId", jiraId);
    }
}

@RestController
@RequiredArgsConstructor
@Slf4j
class JiraToCamundaBridge {

    final ProcessEngine camunda;

    @PostMapping("/jira-webhook")
    void webhook(@RequestBody JiraClient.IssueUpdated msg) {
        log.info("Webhook message : {}", msg);
        if(msg.issueClosed()) {
            camunda
                    .getRuntimeService()
                    .createMessageCorrelation("jira-closed")
                    .processInstanceVariableEquals("jiraId", msg.data.id)
                    .setVariableLocal("jiraReviewer", msg.by.username)
                    .setVariable("approvedInJira", msg.approved())
                    .correlate();
        }
    }
}

@Component
@ConfigurationProperties(prefix = "jira")
@Data
class JiraProps {
    String username, password, apiUrlBase, project, webhookUrlBase;
}


@Configuration
class JiraFactory {

    @Bean
    Jira jira(JiraProps props) {
        var admin = new JiraClient.Auth(props.username, props.password, "normal");
        var client = JiraClient.create(props.apiUrlBase);
        var token = client.auth(admin).auth_token;
        var projectId = client.projectInfo(token, props.project).id;

        var webhookAlreadyRegistered =
                client.webhooks(token, projectId).stream().anyMatch(wh -> "camunda".equals(wh.name));
        if(!webhookAlreadyRegistered)
            client.subscribeWebhook(token, new JiraClient.WebhookSubscription("secret", "camunda", props.webhookUrlBase + "/jira-webhook", projectId));
        return new Jira(client, admin, projectId);
    }
}

record Jira(JiraClient client, JiraClient.Auth principal, int jiraProjectId) {

    JiraClient.IssueInfo createIssue(String title, String description) {
        var token = client().auth(principal()).auth_token;
        return client()
                .createIssue(token, new JiraClient.NewIssueRequest(jiraProjectId(), title, description));
    }

}

interface JiraClient {

    static JiraClient create(String baseUrl) {
        return Feign
                .builder()
                .client(new Http2Client())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(JiraClient.class, baseUrl);
    }

    @Data class AuthResponse { String auth_token; }

    @Data class ProjectInfo { int id; }

    @Data class IssueInfo { int id; }

    @Data class WebhookInfo { int id, project; String name, key; }


    @Data class IssueUpdated {
        @Data static class JiraUser { String username; }
        @Data static class IssueStatus { String slug; boolean is_closed; }
        @Data static class IssueData {int id; IssueStatus status;}

        String action, type; IssueData data; JiraUser by;

        boolean issueClosed() {
            return "change".equals(action) && "issue".equals(type) &&
                    List.of("approved", "rejected").contains(data.status.slug);
        }

        boolean approved() {
            return "approved".equals(data.status.slug);
        }
    }

    @Value class Auth { String username, password, type; }

    @Value class NewIssueRequest { int project; String subject, description; }

    @Value class WebhookSubscription { String key, name, url; int project; }

    @RequestLine("POST /auth")
    @Headers("Content-Type: application/json")
    AuthResponse auth(Auth req);

    @RequestLine("POST /issues")
    @Headers({
            "Content-Type: application/json",
            "Authorization: Bearer {authToken}",
    })
    IssueInfo createIssue(
            @Param("authToken") String authToken,
            NewIssueRequest issue
    );

    @RequestLine("GET /projects/by_slug?slug={projectName}")
    @Headers({
            "Content-Type: application/json",
            "Authorization: Bearer {authToken}",
    })
    ProjectInfo projectInfo(
            @Param("authToken") String authToken,
            @Param("projectName") String projectName
    );

    @RequestLine("POST /webhooks")
    @Headers({
            "Content-Type: application/json",
            "Authorization: Bearer {authToken}",
    })
    WebhookInfo subscribeWebhook(
            @Param("authToken") String authToken,
            WebhookSubscription subscription
    );

    @RequestLine("GET /webhooks?project={projectId}")
    @Headers({
            "Content-Type: application/json",
            "Authorization: Bearer {authToken}",
    })
    List<WebhookInfo> webhooks(
            @Param("authToken") String authToken,
            @Param("projectId") int projectId
    );

}
package bpm.engine;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;

@Component("executionLogger")
@Slf4j
class ExecutionLogger implements ExecutionListener {

    @Override
    public void notify(DelegateExecution execution) {
        log.info("Transfer {} : {}", execution.getProcessBusinessKey(), execution.getCurrentActivityName());
    }
}

plugins {
    java
    id("io.freefair.lombok") version "6.4.3"
    id("org.springframework.boot") version "2.6.8"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
}

tasks.compileJava {
    options.release.set(17)
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":contracts"))

    implementation("org.springframework.boot:spring-boot-starter")

    //jms integration
    implementation("org.springframework.boot:spring-boot-starter-activemq")

    //engine
    implementation(platform("org.camunda.bpm:camunda-bom:7.17.0"))
    implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-rest")
    implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-webapp")
    implementation("org.camunda.bpm:camunda-engine-plugin-spin")
    implementation("org.camunda.spin:camunda-spin-dataformat-all")
    implementation("org.camunda.bpm:camunda-engine-plugin-connect")
    implementation("org.camunda.connect:camunda-connect-http-client")
    //camunda expects a TransactionManager to be on classpath:
    implementation("org.springframework.boot:spring-boot-starter-jdbc")


    //jira integration
    implementation("io.github.openfeign:feign-java11:11.9")
    implementation("io.github.openfeign:feign-jackson:11.9")

    runtimeOnly("com.h2database:h2")
}
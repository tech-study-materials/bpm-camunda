package bpm.audit;

import bpm.camunda_client.model.HistoricDecisionInputInstanceDto;
import bpm.camunda_client.model.HistoricDecisionInstanceDto;
import bpm.camunda_client.model.HistoricProcessInstanceDto;
import j2html.tags.DomContent;
import j2html.tags.specialized.ATag;
import j2html.tags.specialized.TableTag;
import j2html.tags.specialized.TdTag;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;

import static bpm.audit.Formatters.duration;
import static bpm.audit.Formatters.time;
import static bpm.audit.HtmlTable.text;
import static bpm.audit.HtmlTable.*;
import static j2html.TagCreator.*;
import static java.util.Arrays.asList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toMap;

interface Pages {

    interface Processes {
        static String render(Set<String> keys, String processPath, Breadcrumb... breadcrumbs) {
            return renderPageWithBreadcrumbs(
                    htmlTable(
                            keys,
                            List.of(column("", row -> td(a(row).withHref(processPath.formatted(row)))))
                    ),
                    breadcrumbs
            );
        }
    }

    interface BusinessKeys {
        static String render(List<HistoricProcessInstanceDto> keys, String bkPath, Breadcrumb... breadcrumbs) {
            return renderPageWithBreadcrumbs(
                    htmlTable(
                            keys,
                            List.of(
                                    column("b-k", row -> td(a(row.getBusinessKey()).withHref(bkPath.formatted(row.getBusinessKey())))),
                                    text("piid", HistoricProcessInstanceDto::getId),
                                    text("started", time(HistoricProcessInstanceDto::getStartTime)),
                                    text("duration", duration(HistoricProcessInstanceDto::getDurationInMillis))
                            )
                    ),
                    breadcrumbs
            );
        }
    }

    interface AuditLog {

        static List<HtmlTable.Column<Variable>> variablesColumns(String header) {
            return List.of(
                    text(header, Variable::name),
                    text("", Variable::value)
            );
        }

        List<HtmlTable.Column<AuditLogRow>> AUDIT_LOG_COLUMNS = List.of(
                text("activity",             row -> row.activity().getActivityName()),
                text("start time",      time(row -> row.activity().getStartTime())),
                text("duration",    duration(row -> row.activity().getDurationInMillis())),
                text("assignee",             row -> row.activity().getAssignee()),
                column("variables",          row -> td(variablesView(row)))
        );

        static ATag externalLink(String label) {
            return a(join("[", label, rawHtml("&UpperRightArrow;"), "]")).withTarget("_blank");
        }

        static DomContent variablesView(AuditLogRow row) {
            return div(
                    row.vars().isEmpty() ? div() : details().with(summary("[ local vars ]"), htmlTable(row.vars(), variablesColumns(""))),
                    row.decision().map(AuditLog::rulesView).orElse(div())
            );
        }

        static DomContent rulesView(HistoricDecisionInstanceDto decision) {
            var inputs = decision.getInputs().stream()
                    .collect(toMap(
                            HistoricDecisionInputInstanceDto::getClauseName,
                            it -> String.valueOf(it.getValue())))
                    .entrySet().stream().toList();

            return details().with(
                    summary(join(
                            "[ input ]",
                            externalLink("see in cockpit").withHref("http://localhost:8080/camunda/app/cockpit/default/#/decision-instance/" + decision.getId())
                    )),
                    htmlTable(inputs, keyValueColumns("", ""))
            );
        }

        static String render(List<ProcessLog> logs, Breadcrumb... breadcrumbs) {
            var content = div(
                    each(logs, log -> div(
                                    div("processID: %s".formatted(log.piid())),
                                    br(),
                                    details().with(summary("[ process variables ]"), htmlTable(log.unmappedVars(), variablesColumns(""))),
                                    br(),
                                    div("history:"),
                                    htmlTable(log.history(), AUDIT_LOG_COLUMNS).withClass("withhover"),
                                    br(), br()
                            )
                    )
            );
            return renderPageWithBreadcrumbs(content, breadcrumbs);
        }
    }

    static String renderPageWithBreadcrumbs(DomContent content, Breadcrumb[] breadcrumbs) {
        var container = div()
                .with(
                        breadcrumbs(breadcrumbs),
                        br(),
                        br(),
                        content
                );
        return HtmlRenderer.renderPage(container);
    }

    static DomContent breadcrumbs(Breadcrumb[] breadcrumbs) {
        return each(asList(breadcrumbs), crumb -> join("/", a(crumb.label()).withHref(crumb.path())));
    }
}

interface HtmlRenderer {
    String CSS = """
                body {margin: 0; padding: 16px; color:lightgreen; background-color:black; font-family:monospace;}
                table { width: 100%; border-collapse: collapse; }
                tr { border-bottom: 1px solid black; }
                table.withhover tr:hover { border-bottom: 1px solid green; }
                th { text-align: left; border-bottom: 1px solid green;}
                th, td { padding: 0px 8px 0px 8px; ; vertical-align:top; }
                a:link, a:visited { color: lightgreen; text-decoration: none; }
                a:hover, a:active, summary:hover { background-color: lightgreen; color: black; text-decoration: none;}
                summary:focus { outline: none; }
                summary { cursor: pointer; }
            """;

    static String renderPage(DomContent content) {
        return html()
                .with(head().with(style(CSS)))
                .with(body().with(content))
                .renderFormatted();
    }
}

interface Formatters {
    DateTimeFormatter LOCAL_DATE_TIME = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    static <T> Function<T, String> time(Function<T, OffsetDateTime> timeGetter) {
        return a -> LOCAL_DATE_TIME.format(timeGetter.apply(a));
    }

    static <T> Function<T, String> duration(Function<T, Long> timeGetter) {
        return a -> ofNullable(timeGetter.apply(a))
                .map(Duration::ofMillis)
                .map(duration -> String.format("%02d:%02d:%02d.%03d", duration.toHours(), duration.toMinutesPart(), duration.toSecondsPart(), duration.toMillisPart()))
                .orElse("not completed");
    }
}

interface HtmlTable {
    record Column<T>(String header, Function<T, TdTag> cellFactory){}

    static <T> Column<T> column(String header, Function<T, TdTag> cellFactory) {
        return new Column<>(header, cellFactory);
    }

    static <T> Column<T> text(String header, Function<T, String> value) {
        return column(header, row -> td(ofNullable(value.apply(row)).orElse("")));
    }

    static <T> TableTag htmlTable(Collection<T> data, List<Column<T>> columns) {
        return table()
                .with(columns.stream().map(column -> th(column.header())))
                .with(data.stream().map(row -> tr().with(columns.stream().map(column -> column.cellFactory().apply(row)))));
    }

    static List<Column<Map.Entry<String, String>>> keyValueColumns(String keyHeader, String valueHeader) {
        return List.of(
                text(keyHeader,     Map.Entry<String, String>::getKey),
                text(valueHeader,   Map.Entry<String, String>::getValue)
        );
    }
}
package bpm.audit;

import bpm.camunda_client.model.HistoricActivityInstanceDto;
import bpm.camunda_client.model.HistoricDecisionInstanceDto;

import java.util.List;
import java.util.Optional;

record Breadcrumb(String label, String path){}

record Variable(String aiid, String name, String state, String value){}

record AuditLogRow(HistoricActivityInstanceDto activity, Optional<HistoricDecisionInstanceDto> decision, List<Variable> vars){}

record ProcessLog(String piid, List<AuditLogRow> history, List<Variable> unmappedVars){}

package bpm.audit;

import bpm.camunda_client.api.*;
import bpm.camunda_client.invoker.ApiClient;
import bpm.camunda_client.invoker.ApiException;
import bpm.camunda_client.model.*;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

@SpringBootApplication
@RequiredArgsConstructor
@RestController
public class AuditService {

    public static void main(String[] args) {
        SpringApplication.run(AuditService.class, args);
    }

    final ProcessDefinitionApi processesApi;
    final HistoricProcessInstanceApi processHistory;
    final HistoricActivityInstanceApi activityHistory;
    final HistoricDecisionInstanceApi historicDecisions;
    final HistoricVariableInstanceApi historicVariables;

    @GetMapping(value = "/audit-service", produces = TEXT_HTML_VALUE)
    String home() throws ApiException {
        var processes = processesApi
                .getProcessDefinitions(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null)
                .stream()
                .map(ProcessDefinitionDto::getKey)
                .collect(Collectors.toSet());

        return Pages.Processes.render(
                processes,
                "/audit-service/%s",
                new Breadcrumb("Process", "/audit-service"));
    }

    @GetMapping(value = "/audit-service/{processKey}", produces = TEXT_HTML_VALUE)
    String processSelected(@PathVariable String processKey) throws ApiException {
        var bKeys = processHistory
                .queryHistoricProcessInstances(null, null, byProcessKey(processKey))
                .stream()
                .sorted(comparing(HistoricProcessInstanceDto::getStartTime).reversed())
                .toList();

        return Pages.BusinessKeys.render(
                bKeys,
                "/audit-service/"+processKey+"/%s",
                new Breadcrumb("Process", "/audit-service"),
                new Breadcrumb(processKey, "/audit-service/"+processKey)
        );
    }

    @GetMapping(value = "/audit-service/{processKey}/{bk}", produces = TEXT_HTML_VALUE)
    String bkSelected(
            @PathVariable("processKey") String processKey,
            @PathVariable("bk") String bk
    ) throws ApiException {

        var logs = processHistory
                .queryHistoricProcessInstances(null, null, byProcessKeyAndBk(processKey, bk))
                .stream()
                .sorted(comparing(HistoricProcessInstanceDto::getStartTime).reversed())
                .map(HistoricProcessInstanceDto::getId)
                .map(this::fetchProcessLog)
                .toList();

        return Pages.AuditLog.render(
                logs,
                new Breadcrumb("Process", "/audit-service"),
                new Breadcrumb(processKey, "/audit-service/"+processKey),
                new Breadcrumb(bk, "/audit-service/"+processKey + "/" + bk)
        );
    }

    static HistoricProcessInstanceQueryDto byProcessKey(String key) {
        return new HistoricProcessInstanceQueryDto().processDefinitionKey(key);
    }

    static HistoricProcessInstanceQueryDto byProcessKeyAndBk(String pk, String bk) {
        return new HistoricProcessInstanceQueryDto()
                .processDefinitionKey(pk)
                .processInstanceBusinessKeyIn(List.of(bk));
    }

    @SneakyThrows
    ProcessLog fetchProcessLog(String piid) {
        var decisions = decisionsByAIID(piid);
        var vars = variables(piid);
        return new ProcessLog(
                piid,
                activityHistory
                        .queryHistoricActivityInstances(null, null,
                                new HistoricActivityInstanceQueryDto().processInstanceId(piid))
                        .stream()
                        .filter(a -> a.getActivityType()!=null && !a.getActivityType().contains("Gateway"))
                        .sorted(comparing(HistoricActivityInstanceDto::getStartTime))
                        .map(it -> new AuditLogRow(it, ofNullable(decisions.get(it.getId())), vars.claim(it.getId())))
                        .toList(),
                vars.remainder()
        );
    }

    @SneakyThrows
    Map<String, HistoricDecisionInstanceDto> decisionsByAIID(String piid) {
        return historicDecisions
                .getHistoricDecisionInstances(null, null, null, null, null, null, null, null, null, null, piid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, null, null, null)
                .stream()
                .collect(toMap(HistoricDecisionInstanceDto::getActivityInstanceId, identity()))
                ;
    }

    @SneakyThrows
    Variables variables(String piid) {
        var deserializedVars = historicVariables.getHistoricVariableInstances(null, null, null, null, null, null, true, piid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, true);
        var rawVars = historicVariables.getHistoricVariableInstances(null, null, null, null, null, null, true, piid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false);
        return Variables.create(deserializedVars, rawVars);
    }
}

@RequiredArgsConstructor
class Variables {

    private final Map<String, List<Variable>> byAIID;

    static Variables create(
            List<HistoricVariableInstanceDto> deserializedVars,
            List<HistoricVariableInstanceDto> rawVars) {

        var rawVarValuesById = rawVars.stream().collect(toMap(
                HistoricVariableInstanceDto::getId,
                v -> ofNullable(v.getValue()).orElse("???"))
        );

        return new Variables(deserializedVars.stream()
                .map(v -> new Variable(
                        v.getActivityInstanceId(),
                        v.getName(),
                        v.getState(),
                        String.valueOf(ofNullable(v.getValue()).orElse(rawVarValuesById.get(v.getId())))))
                .collect(groupingBy(Variable::aiid)));
    }

    List<Variable> claim(String aiid) {
        return ofNullable(byAIID.remove(aiid)).orElse(Collections.emptyList());
    }

    List<Variable> remainder() {
        return byAIID
                .values()
                .stream()
                .flatMap(Collection::stream)
                .toList();
    }
}

@Configuration
class CamundaRestApis {

    @Bean
    ApiClient apiClient(@Value("${url.engine-rest.base-path}") String baseUri) {
        ApiClient apiClient = new ApiClient();
        apiClient.updateBaseUri(baseUri);
        return apiClient;
    }

    @Bean
    ProcessDefinitionApi pd(ApiClient apiClient) {
        return new ProcessDefinitionApi(apiClient);
    }

    @Bean
    HistoricProcessInstanceApi hpi(ApiClient apiClient) {
        return new HistoricProcessInstanceApi(apiClient);
    }

    @Bean
    HistoricActivityInstanceApi hai(ApiClient apiClient) {
        return new HistoricActivityInstanceApi(apiClient);
    }

    @Bean
    HistoricDecisionInstanceApi hdi(ApiClient apiClient) {
        return new HistoricDecisionInstanceApi(apiClient);
    }

    @Bean
    HistoricVariableInstanceApi hvi(ApiClient apiClient) {
        return new HistoricVariableInstanceApi(apiClient);
    }
}
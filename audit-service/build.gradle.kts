plugins {
    java
    id("io.freefair.lombok") version "6.4.3"
    id("org.springframework.boot") version "2.6.8"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
}

tasks.compileJava {
    options.release.set(17)
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":camunda-rest-client"))
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.j2html:j2html:1.5.0")

}
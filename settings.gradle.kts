rootProject.name = "bpm"
include(
    "jms",
    "contracts",
    "engine",
    "fincrime-starter",
    "sanctions-service",
    "risk-service",
    "document-service",
    "audit-service",
    "camunda-rest-client",
)


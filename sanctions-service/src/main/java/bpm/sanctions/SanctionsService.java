package bpm.sanctions;

import bpm.contracts.sanctions.SanctionsCheckRequest;
import bpm.contracts.sanctions.SanctionsCheckResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import static java.util.concurrent.TimeUnit.SECONDS;


@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
class SanctionsService {

    public static void main(String[] args) {
        SpringApplication.run(SanctionsService.class, args);
    }

    final JmsTemplate jms;

    @JmsListener(destination = "sanctions-check-requests")
    void checkSanctions(SanctionsCheckRequest req) throws InterruptedException {
        log.info("Evaluating if sender of transferID {} is sanctioned...", req.getTransferId());
        SECONDS.sleep(5);
        var response = new SanctionsCheckResponse(req.getTransferId(), req.getTransferId().contains("sanctioned"));
        jms.convertAndSend("sanctions-check-responses", response);
        log.info("Published sanctions check response: {}", response);
    }
}

@Configuration
class JmsConfig {
    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }
}
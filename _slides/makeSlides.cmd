docker run --rm -v "%cd%":/data pandoc/latex:2.14.1 bpm.md -s --webtex -i -t slidy -o bpm.html --metadata title="BPM and orchestrating microservices"
bpm.html
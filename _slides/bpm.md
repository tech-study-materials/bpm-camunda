
* microservice orchestration | choreography
  * suitable for distributed transactions (sagas or LongRunningActions)
  * lifespan expected to be in seconds/minutes - let's not stretch Eventual Consistency too far
  * compensating transactions

* (long) business process management
  * BPM tools (Activiti, Camunda)
  * BPMNotation
  * may involve user actions, so lifespan expected to be in hours/days
  * diminished concept of consistency & compensation in favor of process state & transitions
	
---

Camunda

* Modeler [https://camunda.com/download/modeler/](https://camunda.com/download/modeler/)
* SaaS, Platform 8 [camunda.com](http://camunda.com)
* Embeddable, Platform 7 [docs.camunda.org](https://docs.camunda.org/)
  * can be a monolith or an orchestrator of microservices

* `gradlew :engine:bootRun` -> [localhost:8080](http://localhost:8080/)
* deployed process definition: 
  * engine > Cockpit > Processes > fincrime-workflow
  * [engine/.../resources/fincrime-workflow.bpmn](https://gitlab.com/tech-study-materials/bpm-camunda/-/blob/master/engine/src/main/resources/fincrime-workflow.bpmn)

---

What we're building:
<img src='http://www.plantuml.com/plantuml/svg/ZLJBZfim5DtxAwxSHJ9IWQhBAehKUQYQLbMdJPULaB4lnCMnYQ_fQ3JDlzSmP492L8KDoETXEFUO1OKdWqkMRhJqZj3_qHBXcK6yheMiA-zQgswSSHuQOR3oY9Rjs28RX4mQrofe6meUnGRU21CT2jCYasxJE8ismAFpTMdSOqv1-80U5elnVilbY2jQfKE-VvF_49MJxIO-NCBrckfBU2Ii3DfPkeJCqLRQ8iobsCIGn1K7GJ0idkg6JBMDdr-AQCnlNbJmusxr4pu-t8_KkMXqJo-TKJ4kNiKyGJg5VF3fBeyo25iPXCzVLtdXjQhm3FXRUp56TqVEHQjDIBIbyyxh49fSEclZYlE3VZOso3oIQxt4tc6sjoXrL2kKcheuKxMn9zY-4sdHxB5TB1PJ8eX2q2YWs-jVTjuu2h5y17zX_VubwI7sN1NZrcrRZ5dn60dCOsbNtrTl8Tw_YB8U9jH1PslQ9JL6sEmm3dXtbGpJOQzbJzFbfDjiKlOaMOxc2X-0FtoB8yyQxs9gbALfrbCvEqwx5qvQ_YgNQvHr4d7AdeCNbahqzxSxpexAFjsySSP0WrPfM_K70teduknGerxMvTJnso8MOdi4Bv9a3rCFKyFPcSDpIjkUOqtswio6OzHXXpcpuv_3zDi-tSLWrffYUa-SzVFitc5q13gMZCCB29genO7LMPsm7h5OErTpzWy0'/>

---

Process initiation

* manual: engine > Tasklist > Start process

* via API: 
  * `gradlew :fincrime-starter:bootRun` -> [localhost:8081/swagger-ui](http://localhost:8081/swagger-ui/index.html)
  * POSTs `{"businessKey":"transferId"}` to http://localhost:8080/engine-rest/process-definition/key/fincrime-workflow/start

---

Integration options: http-connector

* `gradlew :document-service:bootRun`, exposes REST API on localhost:8082
* fincrime-workflow.bpmn > check documents
* low-code (possibly no-code) integration
* limited control
* temporal coupling

---

Integration options: engine-specific pub/sub

* publishing tasks: fincrime-workflow.bpmn > check risk
* `gradlew :risk-service:bootRun`
* subscribes to `check-risk-requests` 'topic', completes received tasks
* Camunda's external-task-client SDK [(code)](https://gitlab.com/tech-study-materials/bpm-camunda/-/blob/master/risk-service/src/main/java/bpm/risk/RiskService.java)
* DB + REST disguised as pub/sub (like SQS)
* coupling microservice to Camunda's SDK
* async, no temporal coupling

---

Integration options: send/receive tasks

* fincrime-workflow.bpmn > check sanctions, await sanctions check result
* need [glue code](https://gitlab.com/tech-study-materials/bpm-camunda/-/blob/master/engine/src/main/java/bpm/engine/SanctionsConnector.java) to bridge with async infra (Kafka, SQS, JMS)
* `gradlew :jms:bootRun`, `gradlew :sanctions-service:bootRun`
* microservice not coupled to any Camunda|BPM specifics
* async, no temporal coupling

---

Business process logic

* Gateways (parallel, exclusive)
* decision tables
  * fincrime-workflow.bpmn > evaluate checks
  * engine > Cockpit > Decisions > fincrime checks evaluation
  * [engine/.../resources/fincrime-checks-evaluation.dmn](https://gitlab.com/tech-study-materials/bpm-camunda/-/blob/master/engine/src/main/resources/fincrime-checks-evaluation.dmn)

---

User tasks

* built-in:
  * fincrime-workflow.bpmn > manual review (creates user task)
  * engine > Tasklist or custom GUI (via Camunda REST API)
  * user management, RBAC

* external, e.g.:
  * send task creates jira ticket
  * receive task listens on jira webhook (ticket closed -> task completed)

---

Demo time:

- observe transfers-in-progress in engine > Cockpit > Processes > fincrime-workflow > Process Instances
- transferId:`invalid-doc-transfer` to fail on document check 
- transferId:`risky-transfer` to maybe fail on risk check 
- transferId:`sanctioned-transfer` to fail on sanctions check 

---

Audit and history:

Camunda keeps history tables (which you should purge on schedule) and REST API to access them,
so you can develop a GUI like this one:

- `gradlew :audit-service:bootRun` -> [localhost:8083/audit-service](http://localhost:8083/audit-service)
- works using api client generated from OpenAPI contract [(here)](https://gitlab.com/tech-study-materials/bpm-camunda/-/blob/master/camunda-rest-client/build.gradle.kts)

---

Important, but not covered:

* where's the transfer data that each microservice needs to do the work?
* can be saved in Camunda DB, but good practice is to separate process data from app data
* can be fetched synchronously from transfer-data-service (GET /find-by-transfer-id)
  * heavy load, multiple hits in single process
  * temporal coupling
* can be distributed asynchronously (Kafka|SQS|JMS) to all interested parties upon|before FundsCaptured
  * race condition: BPM action executes before data arrives (repo.findByTransferId() throws NotFound)

---

src: [https://gitlab.com/tech-study-materials/bpm-camunda/-/raw/master/_slides/bpm.md](https://gitlab.com/tech-study-materials/bpm-camunda/-/raw/master/_slides/bpm.md)

<h1>That's all folks</h1>
